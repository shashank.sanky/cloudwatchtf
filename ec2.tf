/*resource "aws_key_pair" "ssh" {
  key_name   = "mykey"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "aws_instance" "web" {
  ami                    = "ami-0c278895328cddfdd"
  instance_type          = "t2.micro"
  //key_name               = "${aws_key_pair.ssh.id}"
  vpc_security_group_ids = [ "${aws_security_group.web.id}" ]
  user_data = <<EOF
  <powershell>
  net user ${var.INSTANCE_USERNAME} ${var.INSTANCE_PASSWORD} /add
  net localgroup administrators ${var.INSTANCE_USERNAME} /add
  </powershell>
  EOF
}

output "web_public_dns" {
  value = "${aws_instance.web.public_dns}"
}

resource "aws_security_group" "web" {
  name        = "webserver1"
  description = "Public HTTP + SSH"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
*/